-- MySQL dump 10.13  Distrib 5.7.16, for Linux (x86_64)
--
-- Host: localhost    Database: worked_base
-- ------------------------------------------------------
-- Server version	5.7.16-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  `summ` int(10) unsigned DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `win` int(10) unsigned NOT NULL,
  `lose` int(10) unsigned NOT NULL,
  `our_procent` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES (2,'SWAMESTREAM',4,0,NULL,0,0,0);
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `streamers`
--

DROP TABLE IF EXISTS `streamers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `streamers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stream_id` int(11) NOT NULL,
  `login` varchar(70) NOT NULL,
  `name` varchar(70) NOT NULL,
  `stream_url` varchar(150) DEFAULT NULL,
  `up_cache` int(10) unsigned NOT NULL DEFAULT '0',
  `contacts` varchar(150) DEFAULT NULL,
  `confidence` int(2) unsigned NOT NULL DEFAULT '1',
  `about` varchar(500) DEFAULT NULL,
  `games_played` int(10) unsigned NOT NULL DEFAULT '0',
  `games_win` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` date DEFAULT NULL,
  `online` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `streamers`
--

LOCK TABLES `streamers` WRITE;
/*!40000 ALTER TABLE `streamers` DISABLE KEYS */;
INSERT INTO `streamers` VALUES (1,27,'qzark1','qzark1','Ссылка на стрим',0,'Контакты',1,'О стриме',0,0,'2016-10-30',0),(2,1,'ourcash','ourcash',NULL,0,NULL,1,NULL,0,0,NULL,0);
/*!40000 ALTER TABLE `streamers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `swamestream0`
--

DROP TABLE IF EXISTS `swamestream0`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `swamestream0` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` tinyint(1) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `swamestream0`
--

LOCK TABLES `swamestream0` WRITE;
/*!40000 ALTER TABLE `swamestream0` DISABLE KEYS */;
INSERT INTO `swamestream0` VALUES (1,1,'qzark1',0,0,'2016-10-24 18:07:25'),(2,1,'qzark1',0,3,'2016-10-24 18:07:39'),(3,1,'qzark1',2,0,'2016-10-24 18:07:42');
/*!40000 ALTER TABLE `swamestream0` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `swamestream1`
--

DROP TABLE IF EXISTS `swamestream1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `swamestream1` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` tinyint(1) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `swamestream1`
--

LOCK TABLES `swamestream1` WRITE;
/*!40000 ALTER TABLE `swamestream1` DISABLE KEYS */;
INSERT INTO `swamestream1` VALUES (1,1,'qzark1',0,0,'2016-10-24 18:08:39'),(2,1,'qzark1',10,0,'2016-10-24 18:08:48'),(3,1,'qzark1',1,1,'2016-10-24 18:09:54');
/*!40000 ALTER TABLE `swamestream1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `swamestream2`
--

DROP TABLE IF EXISTS `swamestream2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `swamestream2` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `swamestream2`
--

LOCK TABLES `swamestream2` WRITE;
/*!40000 ALTER TABLE `swamestream2` DISABLE KEYS */;
INSERT INTO `swamestream2` VALUES (1,2,'qzark1',0,0,'2016-10-24 18:13:44'),(2,2,'qzark1',10,0,'2016-10-24 18:13:50'),(3,2,'qzark1',0,1,'2016-10-24 18:15:00');
/*!40000 ALTER TABLE `swamestream2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `swamestream3`
--

DROP TABLE IF EXISTS `swamestream3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `swamestream3` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `swamestream3`
--

LOCK TABLES `swamestream3` WRITE;
/*!40000 ALTER TABLE `swamestream3` DISABLE KEYS */;
INSERT INTO `swamestream3` VALUES (1,27,'qzark1(S)',0,0,'2016-11-06 20:02:58'),(2,2,'qzark1',0,1,'2016-11-06 20:04:36'),(3,2,'qzark1',10,9,'2016-11-06 20:05:56');
/*!40000 ALTER TABLE `swamestream3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_registration` datetime DEFAULT NULL,
  `stars` double unsigned DEFAULT '0',
  `access` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `steam64` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `code` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'ourcash','ourcash@swame.ru','123',NULL,4,'streamer','0',NULL),(2,'qzark1','qzark1@yandex.ru','$2y$10$mlWqsCaD.r4p2U2PDwt48.zuu2xMvoLCi1L.7WBzXW5gXap9EwXHG','2016-10-24 17:43:44',96,'streamer','0',NULL),(3,'Beyronk','rebit9@yandex.ru','$2y$10$mlWqsCaD.r4p2U2PDwt48.zuu2xMvoLCi1L.7WBzXW5gXap9EwXHG','2016-10-24 22:05:21',0,'usertostrea','0',NULL),(4,'String<script>alert(\'жопа\')</script>','loscript@ya.com','$2y$10$mlWqsCaD.r4p2U2PDwt48.zuu2xMvoLCi1L.7WBzXW5gXap9EwXHG','2016-10-28 19:48:17',0,'user','0',NULL),(9,'test1','test1@swame.ru','$2y$10$tmmUt4ke7NnQtEcIOf2hjeE0NX1kSj375zQ3j5UGQ0425azdxyfi.','2016-10-29 13:20:55',0,'user','0',1768782),(27,'qzark1(S)','0','$2y$10$XXDaQNiTaPGjrDrJjBLccu3VGhL7/K2D8ozDqGC8slUsPuJbM/pYa','2016-10-30 16:10:52',0,'streamer','76561198110793845',NULL),(28,'streamer','streamer@swame.ru','$2y$10$PSZBE5rCnclde0KCwM6y...1sgrZSzHVyob09T2jmLsW6/zdNf1Qy','2016-11-05 16:14:51',0,'usertostrea','0',3506733);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webchat_lines`
--

DROP TABLE IF EXISTS `webchat_lines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webchat_lines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author` varchar(16) NOT NULL,
  `gravatar` varchar(32) NOT NULL,
  `text` varchar(255) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `ts` (`ts`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webchat_lines`
--

LOCK TABLES `webchat_lines` WRITE;
/*!40000 ALTER TABLE `webchat_lines` DISABLE KEYS */;
/*!40000 ALTER TABLE `webchat_lines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webchat_users`
--

DROP TABLE IF EXISTS `webchat_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webchat_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `gravatar` varchar(32) NOT NULL,
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `last_activity` (`last_activity`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webchat_users`
--

LOCK TABLES `webchat_users` WRITE;
/*!40000 ALTER TABLE `webchat_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `webchat_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-18 14:41:51
