-- MySQL dump 10.13  Distrib 5.7.16, for Linux (x86_64)
--
-- Host: localhost    Database: worked_base
-- ------------------------------------------------------
-- Server version	5.7.16-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `streamer` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stream_id` int(11) unsigned NOT NULL,
  `name` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `game` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'other',
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  `summ` int(10) unsigned DEFAULT '0',
  `date_begin` date DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `icon` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'shablon.png',
  `lose` int(11) unsigned DEFAULT '0',
  `win` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `streamer` (`streamer`),
  KEY `game` (`game`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES (2,'',0,'SWAMESTREAM','other',5,0,NULL,NULL,'shablon.png',0,0),(17,'qzark1',2,'qzark1(S)','wow',4,0,'2016-11-20','2016-11-20 03:21:49','shablon.png',0,0);
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qzark1`
--

DROP TABLE IF EXISTS `qzark1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qzark1` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qzark1`
--

LOCK TABLES `qzark1` WRITE;
/*!40000 ALTER TABLE `qzark1` DISABLE KEYS */;
INSERT INTO `qzark1` VALUES (1,0,'All rates',0,0,'2016-11-20 03:21:49');
/*!40000 ALTER TABLE `qzark1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qzark11`
--

DROP TABLE IF EXISTS `qzark11`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qzark11` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `summ` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qzark11`
--

LOCK TABLES `qzark11` WRITE;
/*!40000 ALTER TABLE `qzark11` DISABLE KEYS */;
INSERT INTO `qzark11` VALUES (1,0,'All rates',3,6,'2016-11-19 18:50:07',9),(2,2,'qzark1',0,2,'2016-11-19 18:49:55',NULL),(3,2,'qzark1',0,4,'2016-11-19 18:50:02',NULL),(4,2,'qzark1',3,0,'2016-11-19 18:50:04',NULL);
/*!40000 ALTER TABLE `qzark11` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qzark12`
--

DROP TABLE IF EXISTS `qzark12`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qzark12` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qzark12`
--

LOCK TABLES `qzark12` WRITE;
/*!40000 ALTER TABLE `qzark12` DISABLE KEYS */;
INSERT INTO `qzark12` VALUES (1,0,'All rates',0,0,'2016-11-20 03:02:44');
/*!40000 ALTER TABLE `qzark12` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qzark13`
--

DROP TABLE IF EXISTS `qzark13`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qzark13` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qzark13`
--

LOCK TABLES `qzark13` WRITE;
/*!40000 ALTER TABLE `qzark13` DISABLE KEYS */;
INSERT INTO `qzark13` VALUES (1,0,'All rates',0,0,'2016-11-20 03:11:59');
/*!40000 ALTER TABLE `qzark13` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `streamers`
--

DROP TABLE IF EXISTS `streamers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `streamers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stream_id` int(11) NOT NULL,
  `login` varchar(70) NOT NULL,
  `name` varchar(70) NOT NULL,
  `stream_url` varchar(150) DEFAULT NULL,
  `up_cache` int(10) unsigned NOT NULL DEFAULT '0',
  `contacts` varchar(150) DEFAULT NULL,
  `confidence` int(2) unsigned NOT NULL DEFAULT '1',
  `about` text,
  `games_played` int(10) unsigned NOT NULL DEFAULT '0',
  `games_win` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` date DEFAULT NULL,
  `online` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `streamers`
--

LOCK TABLES `streamers` WRITE;
/*!40000 ALTER TABLE `streamers` DISABLE KEYS */;
INSERT INTO `streamers` VALUES (1,27,'qzark1(S)','qzark1(S)','Ссылка на стрим',0,'Контакты',1,'О стриме',0,0,'2016-10-30',0),(2,1,'ourcash','ourcash',NULL,0,NULL,1,NULL,0,0,NULL,0),(3,2,'qzark1','qzark1(S)','https://www.youtube.com/embed/s4EWpzSNN5s',0,'Контакты',1,'<h3>Добро пожаловать, гайсы.</h3>\r\n<p>Стримы ежедневно с 15-00 до 17-00. <b>Смотреть обязательно.</b></p> Почему не переводится текст на следующую строку я не знаю. Честно.<p><i>А нет, все нормально.</i></p>\r\n\r\n<p>Посмотрим как это все выглядит.</p>',0,0,'2016-10-30',0);
/*!40000 ALTER TABLE `streamers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `swamestream`
--

DROP TABLE IF EXISTS `swamestream`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `swamestream` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `swamestream`
--

LOCK TABLES `swamestream` WRITE;
/*!40000 ALTER TABLE `swamestream` DISABLE KEYS */;
INSERT INTO `swamestream` VALUES (1,2,'qzark1',0,0,'2016-11-19 01:04:46');
/*!40000 ALTER TABLE `swamestream` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_registration` datetime DEFAULT NULL,
  `stars` double unsigned DEFAULT '0',
  `access` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `steam64` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `code` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'ourcash','ourcash@swame.ru','123',NULL,5.2,'streamer','0',NULL),(2,'qzark1','qzark1@yandex.ru','$2y$10$mlWqsCaD.r4p2U2PDwt48.zuu2xMvoLCi1L.7WBzXW5gXap9EwXHG','2016-10-24 17:43:44',95.4,'streamer','0',NULL),(3,'Beyronk','rebit9@yandex.ru','$2y$10$mlWqsCaD.r4p2U2PDwt48.zuu2xMvoLCi1L.7WBzXW5gXap9EwXHG','2016-10-24 22:05:21',0,'usertostrea','0',NULL),(4,'String<script>alert(\'жопа\')</script>','loscript@ya.com','$2y$10$mlWqsCaD.r4p2U2PDwt48.zuu2xMvoLCi1L.7WBzXW5gXap9EwXHG','2016-10-28 19:48:17',0,'user','0',NULL),(9,'test1','test1@swame.ru','$2y$10$tmmUt4ke7NnQtEcIOf2hjeE0NX1kSj375zQ3j5UGQ0425azdxyfi.','2016-10-29 13:20:55',0,'user','0',1768782),(27,'qzark1(S)','0','$2y$10$XXDaQNiTaPGjrDrJjBLccu3VGhL7/K2D8ozDqGC8slUsPuJbM/pYa','2016-10-30 16:10:52',0,'streamer','76561198110793845',NULL),(28,'streamer','streamer@swame.ru','$2y$10$PSZBE5rCnclde0KCwM6y...1sgrZSzHVyob09T2jmLsW6/zdNf1Qy','2016-11-05 16:14:51',0,'usertostrea','0',3506733),(29,'swamestreamer','swame@streamer.ruu','$2y$10$VKyLn5evsWT1mmDFjFCL1OD1lmrfqEdAlb.l/1KzUiC/lW5tNnJEu','2016-11-23 00:35:34',0,'user','0',1346283),(30,'test5','test5@swam.ru','$2y$10$0Y9sBc2Q8vqjsDHQmKvDEOWatD6yMXjDxZ9p43PzB/1BvKXR9/jbS','2016-12-01 07:39:17',0,'usertostreamer','0',3948283),(31,'test6','test6@swm.ru','$2y$10$o5.8pE04bZH2WZq795/ykuOeZ1oM.3crFohSrvJWkqyryNv986F7W','2016-12-01 15:24:40',0,'usertostreamer','0',7473117),(32,'test7','test5@dfs.ru','$2y$10$mnZzBxJdumDf2JU3ilbudu3mPQz1Lbg1GX99NN2bPs9eBAiJq9bga','2016-12-01 16:40:56',0,'usertostreamer','0',7721232),(33,'test8','test5@sdfs.ru','$2y$10$wEOpdkj86NPRy9fWNNoyu.bCOXw4TK5RUvjtkLenz/V4BVAsIRikK','2016-12-01 16:44:01',0,'usertostreamer','0',6893663);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webchat_lines`
--

DROP TABLE IF EXISTS `webchat_lines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webchat_lines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author` varchar(16) NOT NULL,
  `gravatar` varchar(32) NOT NULL,
  `text` varchar(255) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `ts` (`ts`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webchat_lines`
--

LOCK TABLES `webchat_lines` WRITE;
/*!40000 ALTER TABLE `webchat_lines` DISABLE KEYS */;
/*!40000 ALTER TABLE `webchat_lines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webchat_users`
--

DROP TABLE IF EXISTS `webchat_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webchat_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `gravatar` varchar(32) NOT NULL,
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `last_activity` (`last_activity`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webchat_users`
--

LOCK TABLES `webchat_users` WRITE;
/*!40000 ALTER TABLE `webchat_users` DISABLE KEYS */;
INSERT INTO `webchat_users` VALUES (23,'qzark1','','2016-11-15 00:38:54');
/*!40000 ALTER TABLE `webchat_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-04 22:18:15
