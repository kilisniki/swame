<!DOCTYPE HTML>
<?php

	require "../config.php";
	$pagename = '';
	if(!($_SESSION['logged_user']->access=='admin'|
			$_SESSION['logged_user']->access=='moderator')){			
			header('Location: /');
			exit;}
?>
<html>
	<head>
		<title></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="../assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<link rel="stylesheet" href="../assets/css/mystyle_experiment.css" />
		
		
        
        
	</head>
	<body>
		<div id="page-wrapper">
			<!-- Header -->
				<?php require '../header.php';?>
				
				
				
			<!-- Main -->	
		  <div class="wrapper style1" align='middle'>
		  		<h3><a href="startgame.php">Начать игру</a></h3>				
		  		<h3><a href="watchgame.php">Наблюдать за игрой</a></h3>	
		  		<h3><a href="endofgame.php">Закончить игру</a></h3>
		  		<h3><a href="user_to_streamer.php">Апгрейд юзера до стримера</a></h3>			
		  		<h3><a href="get_stars.php">Получить звезды</a></h3>						
		 </div> 



			<!-- Footer -->
				<?php require '../footer.php'?>

				


		<!-- Scripts -->
			<script src="../assets/js/jquery.min.js"></script>
			<script src="../assets/js/jquery.dropotron.min.js"></script>
			<script src="../assets/js/skel.min.js"></script>
			<script src="/assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="../assets/js/main.js"></script>
			

	</body>
</html>