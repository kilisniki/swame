<!DOCTYPE HTML>
<?php


	require "../config.php";

if(!($_SESSION['logged_user']->access=='admin'|
			$_SESSION['logged_user']->access=='moderator')){			
			header('Location: /');
			exit;}
	$pagename = '';
	
	$data = $_POST;
	if(isset($data['earlyend'])) {
		
		//проверка на введенные данные стримера
		if(!$data['stream_login'])
			if(!$data['stream_id']){
				$streamer = $_SESSION['logged_user']->login;
				$findto = 'login';
			}	 	
	 	
	 	//что будем использовать для  поиска стримера в бд
		if($data['stream_id']){ $findto = 'stream_id'; $streamer = $data['stream_id'];}
		if($data['stream_login']){ $findto = 'login'; $streamer = $data['stream_login'];}
			
		//ищем стримера в бд
		$streamer = R::findOne('streamers', $findto.' = ?', array($streamer));
		if($streamer==null){
			$errors = 'Стример не найден';			   
	   }
		
		$GAME = R::findOne($streamer->login, 'id = ?', array('1'));
		if(!$GAME)
			$errors[] = 'Игры не существует';
		if($GAME->date<=date("Y-m-d H:i:s"))
			$errors[] = 'Игра уже закончилась';
			
		if(empty($errors)) {
			//так как у нас данные о конце приема ставок хранится в первой строке таблицы игры, то мы ее изменяем первой
			$GAME->date = date("Y-m-d H:i:s");
			R::store($GAME);
			//но в общей таблице тоже надо изменять
			$game = R::findOne('games', 'streamer = ?', array($streamer->login));
			$game->date_end = date("Y-m-d H:i:s");
			R::store($game);
		}
			
			
	}
	if(isset($data['longend'])) {
		
		//проверка даты
		$timer_min=1;
		$timer_sec=1;
	   if($data['timer_min']){
	   	$timer_min = $data['timer_min'];
	   }
	   if($data['timer_sec']){
	   	$timer_sec = $data['timer_sec'];
	   }
		
		
		//проверка на введенные данные стримера
		if(!$data['stream_login'])
			if(!$data['stream_id']){
				$streamer = $_SESSION['logged_user']->login;
				$findto = 'login';
			}	 	
	 	
	 	//что будем использовать для  поиска стримера в бд
		if($data['stream_id']){ $findto = 'stream_id'; $streamer = $data['stream_id'];}
		if($data['stream_login']){ $findto = 'login'; $streamer = $data['stream_login'];}
		
			
		//ищем стримера в бд
		$streamer = R::findOne('streamers', $findto.' = ?', array($streamer));
		if($streamer==null){
			$errors = 'Стример не найден';			   
	   }
	   
		
		$GAME = R::findOne($streamer->login, 'id = ?', array('1'));
		if(!$GAME)
			$errors[] = 'Игры не существует';
		if(empty($errors)) {
			$format = "Y-m-d H:i:s";
      	$date = date($format);

      	$minutesToAdd = $timer_min;
      	$secondsToAdd = $timer_sec;
      	$interval = "PT{$minutesToAdd}M{$secondsToAdd}S";
      	$date = DateTime::createFromFormat($format, $date);
      	$date->add(new DateInterval($interval));
     
      	
      	//$date = $date->format($format);
      	//echo 'date_end='.$date.' date()='.date($format);

      	
			$GAME->date = $date;
			R::store($GAME);
			//но в общей таблице тоже надо изменять
			$game = R::findOne('games', 'streamer = ?', array($streamer->login));
			$game->date_end = $date;
			R::store($game);
		}

	
	}
?>
<html>
	<head>
		<title></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="../assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<link rel="stylesheet" href="../assets/css/mystyle_experiment.css" />	
        
        
	</head>
	<body>
		<div id="page-wrapper">
			<!-- Header -->
				<?php require '../header.php';?>
				
				
				
			<!-- Main -->	
		  <div class="wrapper style1" align='middle'> 
		  
		  	
					
					
					<p><b style="color: red;"><?php if(!empty($errors)) echo array_shift($errors);?></b></p>
					<form method="POST" action="">
						<p><b>ID стрима</b><br>
						<input type="number" name="stream_id" placeholder="ID"/><br>
						<b>или логин стрима</b><br>					
						<input name="stream_login" placeholder="streamer login"/><br></p>		
					
					<b id="rate_win">На победу: </b><br>
					 <b id="rate_lose">На поражение: </b><br>
					 <b id="game_status"></b>
					 </p>
					 <div style="max-width: 400px;">
					 <p>
					 
					 <input type="submit" class="button my3" name="earlyend" value="Остановить прием"/> 
					 
					 
					 <input type="number" name="timer_min" placeholder="Минуты" size='4'>:
	    			 <input type="number" name="timer_sec" placeholder="Секунды" size='4'><Br>
					 <input type="submit" class="button my3" name="longend" value="Продлить прием"/> 
					 </form>
					 </p>
					 <p>
					 <input  class="button my5" onclick="location.href='endofgame.php'" value="Закончить раунд"/> 
					 </p>
					 <p>					 
					 <input  class="button my4" onclick="location.href='startgame.php'" value="Начать раунд"/> 					 
					 </p>
			</div>	
								
		 </div> 



			<!-- Footer -->
				<?php require '../footer.php'?>

				


		<!-- Scripts -->
			<script src="../assets/js/jquery.min.js"></script>
			<script src="../assets/js/jquery.dropotron.min.js"></script>
			<script src="../assets/js/skel.min.js"></script>
			<script src="/assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="../assets/js/main.js"></script>
			<script><?php echo 'var streamerlogin = "'.$_SESSION['logged_user']->login.'";'?></script>
			<script src="../ajaxclient.js"></script>

	</body>
</html>