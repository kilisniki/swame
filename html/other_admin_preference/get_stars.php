<?php 
	
	require "../config.php";
	
if(!($_SESSION['logged_user']->access=='admin'|
			$_SESSION['logged_user']->access=='moderator')) {			
			header('Location: /');
			exit;}	
	
	$data = $_POST;
	
	if(isset($data['get_stars'])){
		
		if(R::count('users',"login = ?",array($data['login']))==0){
			$errors[] = 'Пользователья с таким login не существует';		
		}
		if(($data['stars']<=0)){
			$errors[] = 'Нельзя отнимать звезды у юзера';
		}
		
		if( empty($errors)) {
			//рег
			
			$user = R::findOne('users', 'login = ?', array($data['login']));
			$user->stars += $data['stars'];
			R::store($user);
			
			echo 'Отлично!<br>Звезды добавлены: login='.$data['login'].' stars='.$data['stars'];
			
		}
		else {
			echo 'Ошибка: ' . array_shift($errors);
		}
		
		
		
		
	
	}

?>


<?php if($_SESSION['logged_user']->access=='admin'|
			$_SESSION['logged_user']->access=='moderator'): ?>
<form action="/other_admin_preference/get_stars.php" method="POST">
	<p><input type="text" name="login" id="login" placeholder="Логин" /></p>
 	<p><input type="number" name="stars" id="stars" placeholder="Звезды" /></p>
 	<p>	<input type="submit" name="get_stars" value="Добавить звезды" /></p>
</form>

<?php else:?>
<p>У вас нет доступа к этой странице</p>
<?php endif;?>