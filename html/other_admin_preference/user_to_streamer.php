<!DOCTYPE HTML>
<?php

	require "../config.php";
	$pagename = 'user_to_streamer';
	
if(!($_SESSION['logged_user']->access=='admin'|
			$_SESSION['logged_user']->access=='moderator')){			
			header('Location: /');
			exit;}	
	$data = $_POST;
	 
	
	if(isset($data['create'])){
	    $OK=0;
	    //ВАЛИДАЦИЯ
	    if(!$data['user_id'])
	        $errors[] = 'Вы не указали id';
	   if(!$data['confidence'])
	        $errors[] = 'Вы не указали уровень доверия';
	        
	   $data['user_id'] = (int) $data['user_id'];                          //всегда предохраняйтесь
	   $data['confidence'] = (int) $data['confidence'];
	   
	   $user = R::findOne('users', 'id = ?', array($data['user_id']));
	   if($user){
	       if(!($user->access=='user' || $user->access=='usertostreamer')){
	           $errors[] = 'Этот пользователь не является простым смертным, апать некуда.';
	       }
	   }
	   else
	    $errors[] = 'Пользователя с таким id='.$$data['user_id'].' не существует';
	    
	       //R::debug( TRUE, 2 ); //select MODE 2 to see parameters filled in
    //R::fancyDebug(); 
	   
	   if(empty($errors)){
	       $streamer = R::dispense('streamers');
	       $streamer->stream_id  = $user->id;
		   $streamer->login  = $user->login;
		   $streamer->name  = $user->login;
		   $streamer->confidence = $data['confidence'];
		  // $streamer->stream_url = 'Ссылка на стрим';
		   //$streamer->contacts = 'Контакты';
		   //$streamer->about = 'О стриме';
		   $streamer->last_activity = $user->date_registration;
		   R::store($streamer);
		   $user->access = 'streamer';
		   R::store($user);
		   $OK = 1;
	   }
	   
	}
	
	
?>
<html>
	<head>
		<title></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="../assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<link rel="stylesheet" href="../assets/css/mystyle_experiment.css" />
	
        
        
	</head>
	<body>
		<div id="page-wrapper">
			<!-- Header -->
				<?php require '../header.php';?>
				
				
				
			<!-- Main -->	
		  <div class="wrapper style1">
		      <div style='text-align:  center;'> 
		  <h2>Апгрейд юзера до стримера</h2>
		  
		  <?php if(!empty($errors)) echo '<a color="red">'.array_shift($errors).'</a>';
		        if($OK) echo '<p><a style="color="green";">Все прошло успешно.</a><br>Убедитесь, что все прошло гладко:<a href="//undegr.ru/streamers/?id='.$streamer->id.'.php">перейдите на страницу стримера</a></p>'?>
		  
		    <form action="" method="POST">
		        <p>Введите id юзера<br>
		            <input type='number' name='user_id' placeholder='id'/>
		        </p>
		        <p>Введите уровень доверия 1-10<br>
		            <input type='number' name='confidence' placeholder='confidence' min='1' max='10'/>
		        </p>
		        <input type="submit" name="create" value="Сделать избранным" />
		    </form>
		    </div>
		    
		
					
				
								
		 </div> 



			<!-- Footer -->
				<?php require '../footer.php'?>

				


		<!-- Scripts -->
			<script src="../assets/js/jquery.min.js"></script>
			<script src="../assets/js/jquery.dropotron.min.js"></script>
			<script src="../assets/js/skel.min.js"></script>
			<script src="/assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="../assets/js/main.js"></script>

	</body>
</html>