<?php
	require "config.php";
	$pagename = 'login';
	if($_SESSION['logged_user']->access=='streamer') {
	header('Location: streamers/streamerpage.php');}
	
	$data = $_POST;
	if(isset($data['do_login'])) {
		$errors = array();
		$user = R::findOne('users', 'login = ?', array($data['login']));
		if ($user){
			if(password_verify($data['password'],$user->password)){
				//все ок логиним
				$_SESSION['logged_user'] = $user;
				$_SESSION['autho'] = 1;
				$data['error_auth'] = NULL;
				//echo 'Успешно! <br>
				//Перейти на <a href="/">главную</a><hr>';
				header('Location: /');	
			}	
			else {
			$errors[] = 'неверный пароль';			
			}		
		}
		else {
			$errors[] = 'пользователя с таким логином не существует';		
		}
		if( !empty($errors)) {
			//результат логина	
			$data['error_auth'] = 'Ошибка:  ' . array_shift($errors);
			
		}
			
	}
	
?>
<!DOCTYPE HTML>
<!--
	Arcana by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>SWAME - Personal Page</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<link rel="stylesheet" href="../assets/css/mystyle_experiment2.css" />
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<?php
				require "header.php";
				?>
			
			
			
			<!-- Main -->
				<section class="myclass">
					<!-- Content -->
						
						
						<img src="images/banner.jpg" alt="" height="220" width="100%"/>
									
 					<section class="myclass style1">
 					<?php	if( isset($_SESSION['logged_user'])) : ?>
 					Вы авторизованны!<br>
	 				Привет, <?php echo $_SESSION['logged_user']->login.'<br>'.
	 				'Ваш ID: '.$_SESSION['logged_user']->id.'<br>'.
	 				'<p>Ваш баланс: '.$_SESSION['logged_user']->stars.'</p>';   ?><br>
	 				
	 				

					
						<a href="logout.php">Выйти</a>
					<?php else : ?>
	
	
	


 						<h3>Вы не авторизованны.</h3>
 						<?php echo '<h1>'.$data['error_auth'].'</h1>'; ?>
 						<form action="login.php" method="POST">
 							<input type="text" name="login" id="email" placeholder="Логин" />
 							<input type="password" name="password" id="email" placeholder="Пароль" />
 						
										
											<input type="submit" class="button my1" name="do_login" value="Авторизация" />
										
							</form>			
											<input type="submit" class="button my2" value="Зарегистрироваться" />
										
									
<?php endif; ?>
 					</section>
							
							
						
				</section>

			<!-- Footer -->
				<?php require 'footer.php'?>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>