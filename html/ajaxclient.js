function CreateRequest()
{
    var Request = false;

    if (window.XMLHttpRequest)
    {
        //Gecko-совместимые браузеры, Safari, Konqueror
        Request = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        //Internet explorer
        try
        {
             Request = new ActiveXObject("Microsoft.XMLHTTP");
        }    
        catch (CatchException)
        {
             Request = new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
 
    if (!Request)
    {
        alert("Невозможно создать XMLHttpRequest");
    }
    
    Request.onreadystatechange = function()
{
    //Если обмен данными завершен
    if (Request.readyState == 4)
    {
        if (Request.status == 200)
        {
            //Передаем управление обработчику пользователя
            r_handler(Request);
        }
        else
        {
            //Оповещаем пользователя о произошедшей ошибке
        }
    }
    else
    {
        //Оповещаем пользователя о загрузке
    }
 
}
    
    return Request;
}
/*
Функция посылки запроса к файлу на сервере
r_method  - тип запроса: GET или POST
r_path    - путь к файлу
r_args    - аргументы вида a=1&b=2&c=3...
r_handler - функция-обработчик ответа от сервера
*/
function SendRequest(r_method, r_path, r_args, r_handler)
{
    //Создаём запрос
    var Request = CreateRequest();
    
    //Проверяем существование запроса еще раз
    if (!Request)
    {
        return;
    }
    
    //Назначаем пользовательский обработчик
    Request.onreadystatechange = function()
    {
        //Если обмен данными завершен
        if (Request.readyState == 4)
        {
            //Передаем управление обработчику пользователя
            r_handler(Request);
        }
    }
    
    //Проверяем, если требуется сделать GET-запрос
    if (r_method.toLowerCase() == "get" && r_args.length > 0)
    r_path += "?" + r_args;
    
    //Инициализируем соединение
    Request.open(r_method, r_path, true);
    
    if (r_method.toLowerCase() == "post")
    {
        //Если это POST-запрос
        
        //Устанавливаем заголовок
        Request.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
        //Посылаем запрос
        Request.send(r_args);
    }
    else
    {
        //Если это GET-запрос
        
        //Посылаем нуль-запрос
        Request.send(null);
    }
}


var rate_win=0;
var rate_lose=0;

//FUNCTION
function ReadFile(filename, container)
{
    //Создаем функцию обработчик
    var Handler = function(Request)
    {
        //document.getElementById('json').innerHTML = Request.responseText;
        console.log(Request.responseText);
	var responsedata = JSON.parse(Request.responseText);
	//var responsedata = eval(Request.responseText);
	//console.log(Request.responseText);
        //document.getElementById(container).innerHTML = responsedata.data.game.name;
        document.getElementById('rate_win').innerHTML = "На победу: "+responsedata.data.game.win;
        document.getElementById('rate_lose').innerHTML = "На поражение: "+ responsedata.data.game.lose;
        
        if(responsedata.data.game.date!=null){
        //var text = "2012-11-28 12:12:45";
        
        var date_end = new Date(responsedata.data.game.date.replace(/(\d+)-(\d+)-(\d+)(\s)(\d+):(\d+):(\d+):(\d+)/, '$1-$2-$3 $4:$5:$6 '));
        var date_now = new Date(responsedata.data.time_server.replace(/(\d+)-(\d+)-(\d+)(\s)(\d+):(\d+):(\d+):(\d+)/, '$1-$2-$3 $4:$5:$6 '));
        var delta = date_now.valueOf()-date_end.valueOf();
        if(delta>0){
            document.getElementById('game_status').style.color = "red";
            document.getElementById('game_status').innerHTML = 'Прием ставок завершен.';
        }
        else{
            document.getElementById('game_status').style.color = "green";
            document.getElementById('game_status').innerHTML = 'Прием ставок открыт.';
        }
        }
        if(responsedata.data.game.date==null || responsedata.data.game.date==''){
        document.getElementById('game_status').style.color = "#2b82d9";
        document.getElementById('game_status').innerHTML = 'Игра еще не началась.';
        }
        
        rate_win = responsedata.data.game.win;
        rate_lose = responsedata.data.game.lose;
        
        
        
        
//        var datenow = Date();        
 //       var dateend = Date(responsedata.data.game.date_end.replace(/(\d+)-(\d+)-(\d+)/,'$2/$3/$1'));
        // var format = Date("YYYY-MM-DD HH:mm:ss");
         //document.getElementById('game_status').innerHTML = date;
    }
    //var streamerlogin = 'undegrstream';
    //Отправляем запрос
    SendRequest("POST",filename,"a="+streamerlogin,Handler);
    
}
ReadFile("../ajax_server.php","switch");


//дожидаемся полной загрузки страницы
window.onload = function () {

    //получаем идентификатор элемента
    var a = document.getElementById('switch');
    
    //вешаем на него событие
console.log(Request.responseText);    a.onclick = function() {
    		
    		ReadFile("../ajax_server.php","switch");
    }
    
}
var timerId = setInterval(function() {
  ReadFile("../ajax_server.php","switch");
}, 5000);

var inputwin = document.getElementById('win');
var inputlose = document.getElementById('lose');

 inputwin.oninput = function() {
    var proc = +inputwin.value/(Number(rate_win)+Number(inputwin.value));
    var lose = +rate_lose*0.8;
    var total = +inputwin.value + (lose)*proc;
    if(!isFinite(total)) total = 0.0;
    document.getElementById('total_win').innerHTML = +total.toFixed(2);
    
 };
 inputlose.oninput = function() {
    var proc = +inputlose.value/(Number(rate_lose)+Number(inputlose.value));
    var lose = +rate_win*0.8;
    var total = +inputlose.value + (lose*proc);
    if(!isFinite(total)) total = 0.0;
    document.getElementById('total_lose').innerHTML = +total.toFixed(2);
    
 };













