<!DOCTYPE HTML>
<?php

	require "../config.php";
	$pagename = '';
	
	$data=$_POST;
	
	if(isset($data['send'])){
		
	    if($_SESSION['logged_user']->login==null)
	    $errors[] = 'Вы не авторизированы';
	    if(!($_SESSION['logged_user']->access=='user'))
	    $errors[] = 'Вы не являетесь обычным пользователем.<br>Если вы раньше не отправляли заявку, обратитесь в <a href="//undegr.ru/about/contacts.php">службу поддержки</a>';
	    if(strlen($data['streamer_contacts'])<5)
	    $errors[] = 'Вы не указали контакты';
	    if(strlen($data['streamer_url'])<5)
	    $errors[] = 'Вы не указали ссылки на стримы';
	    if(strlen($data['streamer_time'])<5)
	    $errors[] = 'Вы не указали время стримов';
	    if(strlen($data['streamer_about'])<5)
	    $errors[] = 'Вы не рассказали о себе, сколько лет и в какие игры вы играете?';
	    
	    if(empty($errors)){
	        $user = R::findOne('users', 'id = ?', array($_SESSION['logged_user']->id));
	        echo $_SESSION['logged_user']->id.'<br>'.$user;
	        $user->access = 'usertostreamer';
	        R::store($user);
	        
	        //отправляем мыло
	        $to  = "ancets@undegr.ru" ; 
            
            $subject = 'Заявка на стрим от '.$user->login; 
            $message = ' 
                        <html> 
                        <head> 
                            <title>Заявка на стрим от '.$user->login.'</title> 
                        </head> 
                        <body> 
                            <p>'
                            .'Логин:'.$user->login.'<br>'
                            .'ID:'.$user->id.'<br>'
                            .'Email:'.$user->email.'<br>'
                            .'<p>О себе:<br>'.$data['streamer_about'].'</p>'
                            .'<p>Cтримы:<br>'.$data['streamer_url'].'</p>'
                            .'<p>Время стримов:<br>'.$data['streamer_time'].'</p>'
                            .'<p>Контакты:<br>'.$data['streamer_contacts'].'</p>'
                            .'</p> 
                        </body> 
                        </html>'; 

            $headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
            $headers .= "From: SWAME-information <inform@undegr.ru>\r\n"; 
            $headers .= "Bcc: ancets-archive@undegr.ru\r\n"; 
            if(!(mail($to, $subject, $message, $headers)))
                $errors[] = 'Не удалось отправить письмо'; 
	        
	    }
	}
?>
<html>
	<head>
		<title></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="../assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<link rel="stylesheet" href="../assets/css/mystyle_experiment.css" />
		
		
        <style>
   #quest {
    width: 500px; /* Ширина блока */
     /* Цвет фона */
     /* Поля вокруг текста */
    border: 1px solid #ccc; /* Параметры рамки */
    padding: 1px;
   }
   #result {
       text-decoration: none; /* Убираем подчеркивание */
       border-bottom: none;
       color: red;
   }
   #result2 {
       text-decoration: none; /* Убираем подчеркивание */
       border-bottom: none;
       color: green;
   }
  </style>
        
	</head>
	<body>
		<div id="page-wrapper">
			<!-- Header -->
				<?php require '../header.php';?>
				
				
				
			<!-- Main -->	
		  
		    <div align="center" style='padding: 50px 0 50px 0'>
		        <h2>Анкета на участие в проекте</h2>
		        <?php if(isset($data['send']) && empty($errors))
		                echo '<a id="result2">Заявка отправлена</a>';
		              else
		                echo '<a id="result">'.array_shift($errors).'</a>';
		        ?>
		        <form action='' method='POST'>
		            <p>Укажите контакты по которым мы сможем с вами связаться<br>
		            <textarea id='quest' name='streamer_contacts' rows=4><?php echo $data['streamer_contacts']?></textarea></p>
		            <p>Добавьте сюда ссылки на ваши стримы<br>
		            <textarea id='quest' name='streamer_url' rows=3 ><?php echo $data['streamer_url']?></textarea></p>
		            <p>В какое время вы обычно стримите?<br>
		            <textarea id='quest' name='streamer_time' rows=3 ><?php echo $data['streamer_time']?></textarea></p>
		            <p>Расскажите о себе и вашей карьере стримера.<br> Больше информации поможет нам быстрее рассмотреть заявку.<br>
		            <textarea id='quest' name='streamer_about' rows=7 ><?php echo $data['streamer_about']?></textarea></p>
		            
		            <input type='submit' name='send' value='Отправить заявку'/>
		            
		        </form>
		    </div>
					
				
								
		 



			<!-- Footer -->
				<?php require '../footer.php'?>

				


		<!-- Scripts -->
			<script src="../assets/js/jquery.min.js"></script>
			<script src="../assets/js/jquery.dropotron.min.js"></script>
			<script src="../assets/js/skel.min.js"></script>
			<script src="/assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="../assets/js/main.js"></script>
			
			

	</body>
</html>