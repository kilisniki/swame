<!DOCTYPE HTML>
<?php


	require "../config.php";

if(!($_SESSION['logged_user']->access=='admin'|
			$_SESSION['logged_user']->access=='moderator'|
			$_SESSION['logged_user']->access=='streamer'))
			header('Location: /');

	$pagename = '';
	
	$data = $_POST;
	if(isset($data['earlyend'])) {
		$GAME = R::findOne($_SESSION['logged_user']->login, 'id = ?', array('1'));
		if(!$GAME || $GAME->date<=date("Y-m-d H:i:s"))
			$errors[] = 'Игра уже закончилась';
		else{
			//так как у нас данные о конце приема ставок хранится в первой строке таблицы игры, то мы ее изменяем первой
			$GAME->date = date("Y-m-d H:i:s");
			R::store($GAME);
			//но в общей таблице тоже надо изменять
			$game = R::findOne('games', 'streamer = ?', array($_SESSION['logged_user']->login));
			$game->date_end = date("Y-m-d H:i:s");
			R::store($game);
		}
			
			
	}
?>
<html>
	<head>
		<title></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="../assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<link rel="stylesheet" href="../assets/css/mystyle_experiment.css" />	
        
        
	</head>
	<body>
		<div id="page-wrapper">
			<!-- Header -->
				<?php require '../header.php';?>
				
				
				
			<!-- Main -->	
		  <div class="wrapper style1" align='middle'> 
		  
		  	
					<p>
					<?php if(!empty($errors)) echo '<b style="color: red;">'.array_shift($errors).'</b><br>';?>
					<b id="rate_win">На победу: </b><br>
					 <b id="rate_lose">На поражение: </b><br>
					 <b id="game_status"></b>
					 </p>
					 <div style="max-width: 400px;">
					 <p>
					 <form method="POST" action="">
					 <input type="submit" class="button my3" name="earlyend" value="Остановить прием"/> 
					 </form>
					 </p>
					 <p>
					 <input type="submit" class="button my5" onclick="location.href='endofgame.php'" value="Закончить раунд"/> 
					 </p>
					 <p>					 
					 <input type="submit" class="button my4" onclick="location.href='startgame.php'" value="Начать раунд"/> 					 
					 </p>
			</div>	
								
		 </div> 



			<!-- Footer -->
				<?php require '../footer.php'?>

				


		<!-- Scripts -->
			<script src="../assets/js/jquery.min.js"></script>
			<script src="../assets/js/jquery.dropotron.min.js"></script>
			<script src="../assets/js/skel.min.js"></script>
			<script src="/assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="../assets/js/main.js"></script>
			<script><?php echo 'var streamerlogin = "'.$_SESSION['logged_user']->login.'";'?></script>
			<script src="../ajaxclient.js"></script>

	</body>
</html>