<?php

	
	   //убираем пробелы
		$data['streamer_url'] = trim($data['streamer_url']);
		$data['streamer_streamname'] = trim($data['streamer_streamname']);
		$data['streamer_about'] = trim($data['streamer_about']);


		 if($_SESSION['logged_user']->login==null)	
	    $errors[] = 'Вы не авторизированы';
	    if(!($_SESSION['logged_user']->access=='streamer'))
	    $errors[] = 'Вы не являетесь стримером';
	    if(valid_url(data['streamer_url'])===null)
	    $errors[] = 'Вы указали неправильную ссылку на стрим.
	    <br> Как указывать правильно 
	    <a href="//undegr.ru/about/howvalignurl.php">ссылку</a>.';
	    if($resval = valid_name($data['streamer_streamname'])) {
			if($resval==1)
			$errors[]='Неправильный формат имени стрима';
			if($resval==2)
			$errors[]='Слишком длинное имя стрима';	    
	    }
	    if($resval = valid_about($data['streamer_about'])){
	    	if($resval==1)
			$errors[]='К сожалению, пока что в поле "о стриме" можно использовать только буквы, цифры и некоторые знаки препинания.';
			if($resval==2)
			$errors[]='Описание стрима должно умещаться в 500 символов.';    
	    }
	    
	    
	    
	    if(empty($errors)) {
	    $streamer = R::findOne('streamers', 'stream_id = ?', array($_SESSION['logged_user']->id));
	    if($streamer) {
	    	$streamer->name = $data['streamer_streamname'];
			$streamer->about = $data['streamer_about'];
			$streamer->stream_url = $data['streamer_url'];
			R::store($streamer);	    
	    //header('Location: /streamers/streamerpage.php');
	 	 }	 
	 } 
	 
	    
	    
	    
	    
function valid_url($url) {
	$valid = 0;	
	$valid += preg_match('(^(http)(s?)://www.youtube.com(/[a-zA-Z0-9]*){1,}$)', $url);//youtube
	$valid += preg_match('(^(http)(s?)://player.twitch.tv(/\?channel=[a-zA-Z0-9]*)$)', $url);//twitch
	$valid += preg_match('(^(http)(s?)://goodgame.ru(/player\?[a-zA-Z0-9]*)$)', $url);//gg
	return $valid;
}	  
function valid_name($name) {
	if(!preg_match('/^(([a-zA-Zа-яА-ЯёЁ0-9]*[\-]?[,()\.!\?_\s]*([a-zA-Zа-яА-ЯёЁ0-9])+)*[\-]?[(),\.!\?_\s]*)$/u', $name))
		return 1;
	if(strlen($name)>70)
		return 2;
	return 0;
}   
function valid_about($about) {
	if(!preg_match('/^(([a-zA-Zа-яА-ЯёЁ0-9]*[\-]?[,()\.!\?_\s:(<p>)(<\/p>)(<a>)(<\/a>)(<b>)(<\/b>)(<i>)(<\/i>)(<h3>)(<\/h3>)]*([a-zA-Zа-яА-ЯёЁ0-9])+)*[\-]?[(),\.!\?_\s:(<p>)(<\/p>)(<a>)(<\/a>)(<b>)(<\/b>)(<i>)(<\/i>)(<h3>)(<\/h3>)]*)$/u', $about))
		return 1;
	if(strlen($about)>500)
		return 2;
	return 0;
}

?>