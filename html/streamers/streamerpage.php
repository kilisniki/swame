<?php
	require "../config.php";
	$pagename = 'login';
	if(!$_SESSION['logged_user']->access=='streamer')
	header('Location: ../personalpage.php');
	else 
	$_SESSION['logged_streamer'] = R::findOne('streamers', 'stream_id = ?', array($_SESSION['logged_user']->id));
	
	$data = $_POST;
	if(isset($data['do_login'])) {
		$errors = array();
		$user = R::findOne('users', 'login = ?', array($data['login']));
		if ($user){
			if(password_verify($data['password'],$user->password)){
				//все ок логиним
				$_SESSION['logged_user'] = $user;
				$_SESSION['autho'] = 1;
				$data['error_auth'] = NULL;
				//echo 'Успешно! <br>
				//Перейти на <a href="/">главную</a><hr>';
				header('Location: /');	
			}	
			else {
			$errors[] = 'неверный пароль';			
			}		
		}
		else {
			$errors[] = 'пользователя с таким логином не существует';		
		}
		if( !empty($errors)) {
			//результат логина	
			$data['error_auth'] = 'Ошибка:  ' . array_shift($errors);
		}
	}
	
	if(isset($data['send'])){
		require "valid.php";
	}
?>
<!DOCTYPE HTML>

<html>
	<head>
		<title>SWAME - Personal Page</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="//undegr.ru/assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<link rel="stylesheet" href="//undegr.ru/assets/css/mystyle_experiment2.css" />
	</head>
	 <style>
   #formarea {
	padding: 0px 0 50px 0;  
   }
   #quest {
    width: 500px; /* Ширина блока */
     /* Цвет фона */
     /* Поля вокруг текста */
    border: 1px solid #ccc; /* Параметры рамки */
    padding: 1px;
   }
   #result {
       text-decoration: none; /* Убираем подчеркивание */
       border-bottom: none;
       color: red;
   }
   #result2 {
       text-decoration: none; /* Убираем подчеркивание */
       border-bottom: none;
       color: green;
   }
  </style>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<?php	require "../header.php";?>
			
			
			
			<!-- Main -->
				<section class="myclass">			
					<img src="//undegr.ru/images/banner.jpg" alt="" height="220" width="100%"/>
					<section class="myclass style2">

 					<?php	if( isset($_SESSION['logged_user'])) : ?>
 						Вы авторизованны!<br>
	 					Привет, <strong><?php echo $_SESSION['logged_user']->login.
	 					'Ваш ID: '.$_SESSION['logged_user']->id.'
	 					<p>Ваш баланс: '.$_SESSION['logged_user']->stars.'</p>';   
	 				?><br>
	 				
	 					 <div id="formarea">
	 					 	<h3>Информация о стриме.</h3>
	 						<?php if(isset($data['send']) && empty($errors))
	 									echo '<a id="result2">Информация сохранена</a>';
		              			else
		           			 		echo '<a id="result">'.array_shift($errors).'</a>';?>		           			 
		        			<form action='' method='POST'>
		            		<p><strong>Название стрима.</strong><br>
		            		<input id="quest" name="streamer_streamname" <?php echo 'value="'.$_SESSION['logged_streamer']->name.'"';?> maxlength="70" /><br>
		            		<i>Рекомендуется поставить ваш никнейм, используемый на стриминговых сервисах.</i></p>
		            		<p><strong>Ссылка на Ваш стрим</strong><br>
		            		<textarea id='quest' name='streamer_url' rows=3 maxlength="150" ><?php if(empty($errors)) echo $_SESSION['logged_streamer']->stream_url; else $data['streamer_url'];?></textarea></p> 
		            		<p><strong>Информация о стриме.</strong><br><i>Будет отображаться на странице стрима.</i><br>
		            		<textarea id='quest' name='streamer_about' rows=7 maxlength="500"><?php if(empty($errors)) echo $_SESSION['logged_streamer']->about; else $data['streamer_about'];?></textarea></p>
		            		<input type='submit' name='send' value='Сохранить'/>
		        			</form>
		        		</div>
	 					<a href="logout.php">Выйти из аккаунта</a>
	 				
					<?php else : ?>
 						<h3>Вы не авторизованны.</h3>
 						<?php echo '<h1>'.$data['error_auth'].'</h1>'; ?>
 						<form action="login.php" method="POST">
 							<input type="text" name="login" id="email" placeholder="Логин" />
 							<input type="password" name="password" id="email" placeholder="Пароль" />
 							<input type="submit" class="button my1" name="do_login" value="Авторизация" />
						</form>			
						<input type="submit" class="button my2" value="Зарегистрироваться" />
					<?php endif; ?>
 					</section>
							
							
						
				</section>

			<!-- Footer -->
				<?php require '../footer.php'?>

		</div>

		<!-- Scripts -->
			<script src="//undegr.ru/assets/js/jquery.min.js"></script>
			<script src="//undegr.ru/assets/js/jquery.dropotron.min.js"></script>
			<script src="//undegr.ru/assets/js/skel.min.js"></script>
			<script src="//undegr.ru/assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="//undegr.ru/assets/js/main.js"></script>

	</body>
</html>