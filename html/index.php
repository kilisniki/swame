<!DOCTYPE HTML>
<?php

	require "config.php";
	$pagename = 'index';
?>
<html>
	<head>
		<title>Swame - Главная страница</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<link rel="stylesheet" href="../assets/css/mystyle_experiment2.css" />
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<?php
				require "header.php";
				?>

			<!-- Banner -->
				<section id="banner">
					<header>
						<h2>Swame: <em>верь в удачу и выигрывай!</em></h2>
						<a href="about.html" class="button">Узнать больше</a>
					</header>
				</section>

			<!-- Highlights -->
				<section class="wrapper style1">
					<div class="container">
						<div class="row 200%">
							<section class="4u 12u(narrower)">
								<div class="box highlight">
									<i class="icon major fa-comment"></i>
									<h3>Общайся!</h3>
									<p>Делись впечатлениями от игры с другими людьми в общем чате!</p>
								</div>
							</section>
							<section class="4u 12u(narrower)">
								<div class="box highlight">
									<i class="icon major fa-rub"></i>
									<h3>Выигрывай!</h3>
									<p>Если ты уверен в результате матча, ты можешь сделать ставку!</p>
								</div>
							</section>
							<section class="4u 12u(narrower)">
								<div class="box highlight">
									<i class="icon major fa-video-camera"></i>
									<h3>Показывай!</h3>
									<p>Ты тоже можешь стать стримером, просто отправь заявку!</p>
								</div>
							</section>
						</div>
					</div>
				</section>

			<!-- Gigantic Heading -->
				<section class="wrapper style2">
					<div class="container">
						<header class="major">
							<h2>Мы запускаем новый уникальный проект!</h2>
							<p>Стример? Ютубер? Хороший игрок? Киберспортсмен? Напиши нам. </p>
						</header>
					</div>
				</section>

			<!-- Posts -->
				<section class="wrapper style1">
					<div class="container">
						<div class="row">
							<section class="6u 12u(narrower)">
								<div class="box post">
									<a href="#" class="image left"><img src="images/pic01.jpg" alt="" /></a>
									<div class="inner">
										<h3>Мы отрылись</h3>
										<p>Ознакомьтесь с правилами сайта и наслаждайтесь</p>
									</div>
								</div>
							</section>
							<section class="6u 12u(narrower)">
								<div class="box post">
									<a href="#" class="image left"><img src="images/pic02.jpg" alt="" /></a>
									<div class="inner">
										<h3>Стримеры</h3>
										<p>Список всех стримеров, участвующих в нашем проекте</p>
									</div>
								</div>
							</section>
						</div>
						<div class="row">
							<section class="6u 12u(narrower)">
								<div class="box post">
									<a href="#" class="image left"><img src="images/pic03.jpg" alt="" /></a>
									<div class="inner">
										<h3>Ютуберы</h3>
										<p>Список ютуб каналов, с которыми мы сотрудничаем</p>
									</div>
								</div>
							</section>
							<section class="6u 12u(narrower)">
								<div class="box post">
									<a href="#" class="image left"><img src="images/pic04.jpg" alt="" /></a>
									<div class="inner">
										<h3>Игроки</h3>
										<p>Топ популярных игроков</p>
									</div>
								</div>
							</section>
						</div>
					</div>
				</section>

			<!-- CTA -->
				<section id="cta" class="wrapper style3">
					<div class="container">
						<header>
							<h2>Хотите попытать удачу?</h2>
							<a href="login.php" class="button">Регистрация</a>
						</header>
					</div>
				</section>

			<!-- Footer -->
				<?php require 'footer.php'; ?>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>
