<?php
	require "config.php";
	$pagename = 'login';
	
	$data = $_POST;
	if(isset($data['do_signup'])) {
		$ok=0;
		
		//проверки на пустоту
		if( trim($data['login']) == '')
		{
			$errors[] = 'Введите логин';
		}	
		if( trim($data['email']) == '')
		{
			$errors[] = 'Введите email';
		}	
		if( $data['password'] == '')
		{
			$errors[] = 'Ведите пароль';
		}	
		if(R::count('users',"login = ?",array($data['login']))>0){
			$errors[] = 'Пользователь с таким login уже существует';		
		}
		if(R::count('users',"email = ?",array($data['email']))>0){
			$errors[] = 'Пользователь с таким email уже существует';		
		}
		if( $data['password'] != $data['password2'])
		{
			$errors[] = 'Пароли не совпадают';
		}
		if( !preg_match('/^[a-zA-Z0-9]{5,16}$/',trim($data['login'])))
		{
			$errors[] = 'Логин может содержать только латинские буквы и цифры и его длина должна быть от 5 до 16 символов';
		}
		if( !preg_match('/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,8}$/',trim($data['email'])))
		{
			$errors[] = 'Неправильный формат email';
		}	
			
		if( empty($errors)) {
			//регистрация
			
			$user = R::dispense('users');
			$user->login = $data['login'];
			$user->email = $data['email'];
			$user->password = password_hash($data['password'], PASSWORD_DEFAULT);
			$user->date_registration = date("Y-m-d H:i:s");
			$user->stars = 0;
			$user->code = rand(100000, 9999999);
			R::store($user);
			$ok=1;
			//echo 'Отлично!';
			//header('Location: /');
			
		}
		//else {
		//	echo 'Ошибка: ' . array_shift($errors);
		//}
		
	
			
	}
	
?>
<!DOCTYPE HTML>
<!--
	Arcana by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Swame - Регистрация</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="//undegr.ru/assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<link rel="stylesheet" href="//undegr.ru/assets/css/mystyle_experiment2.css" />
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<?php
				require "header.php";
				?>
			
			
			
			<!-- Main -->
				<section class='myclass'>
					<!-- Content -->
						
						
						<img src="images/banner.jpg" alt="" height="220" width="100%"/>
						<section class='myclass style1'>		
 					<?php	if( isset($_SESSION['logged_user'])) : ?>
 					Вы авторизованны!<br>
	 				Привет, <?php echo $_SESSION['logged_user']->login;  ?>!<br>

					
						<a href="logout.php">Выйти</a>
					<?php else : ?>
	
	
	


 						<h3>Регистрация на сайте.</h3>
 						
 						<?php echo '<h1 style="color: #f80000">'.array_shift($errors).'</h1>'; 
 						        if(empty($errors)&&$ok) echo '<h1 style="color: #66ff33">Регистрация прошла успешно, перейдите по ссылке в письме для активации аккаунта(нет)</h1>'?>
 						<form action="" method="POST">
							<p><input type="text" name="login" id="login" placeholder="Логин" value="<?php echo @$data['login'];?>"/></p>
							<p><input type="email" name="email" id="email" placeholder="Почта" value="<?php echo @$data['email'];?>"/></p>

 							<p><input type="password" name="password" id="password" placeholder="Пароль"/></p>
 							<p><input type="password" name="password2" id="password2" placeholder="Повторите пароль" /></p>
 						
										<i>Нажимая кнопку зарегистрироваться, Вы принимаете условия пользовательского <a href='/about/rules.php'>соглашения.</a></i>
											<p><input type="submit" class="button my2" name="do_signup" value="Зарегистрироваться" /></p>
										
							
									
                        <?php endif; ?>
 					
							
					</section>			
						
				</section>

			<!-- Footer -->
				<?php require 'footer.php'?>

		</div>

		<!-- Scripts -->
			<script src="//undegr.ru/assets/js/jquery.min.js"></script>
			<script src="//undegr.ru/assets/js/jquery.dropotron.min.js"></script>
			<script src="//undegr.ru/assets/js/skel.min.js"></script>
			<script src="//undegr.ru/assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="//undegr.ru/assets/js/main.js"></script>

	</body>
</html>
