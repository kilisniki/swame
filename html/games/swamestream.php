<!DOCTYPE HTML>
<?php

	require "../config.php";
	$pagename = 'swamestream';
	
	$streamer = $_GET['str'];
	//if(!preg_match('(^([a-zA-Z0-9()]+)$)', $streamer))
	//	$streamer = 'qzark1'; 
		
	require 'ratedo.php';
	
	
	//$game = R::findOne('games', 'streamer = ?', array($streamer));
	$game = R::findOne('streamers', 'login = ?', array($streamer));
?>
<html>
	<head>
		<title>Swame - SwameStream</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="../assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<link rel="stylesheet" href="../assets/css/mystyle_experiment.css" />
		
		


        <style>
  #mychatcontainer2 {
    -position: relative;
    min-height: 500px; /* Or whatever you want (eg. 400px) */
    width: 100%;
    overflow-y: scroll;
    overflow-x: hidden;    
  }
  #chatContainer {
	color: #ffffff;
	padding: 0 5px 0 5px;
}
</style>
       
        
	</head>
	<body>
		<div id="page-wrapper">
			<!-- Header -->
				<?php require '../header.php';?>
				
				
				
			<!-- Main -->	
		 <!-- <div class="wrapper style1">  -->
			<div class="my-flex-container">
			<!-- ПЕРВЫЙ ЛЕВЫЙ БЛОК АВТОРИЗАЦИЯ И СТАВКИ -->
			<div class="my-flex-block1">
				<?php if(isset($_SESSION['logged_user'])): ?>
					<!-- <h3>Сделайте ставку!</h3> -->
					<h1><b>Ваш баланс: <?php echo $_SESSION['logged_user']->stars; ?></b></h1>
               <form action="" method="POST">    
						<p><input type="number" style="width: 100%;" name="win" id="win" placeholder="На победу" min="0"/>
						Выигрыш: <b id="total_win"></b>
						<input type="number" style="width: 100%;" name="lose" id="lose" placeholder="На поражение" min="0"/>
						Выигрыш: <b id="total_lose"></b>
						<input type="submit" class="button my1" name='do_rate' value="Сделать ставку"/></p>
					</form>
				<?php else: ?>
				
					
					<form action="../login.php" method="POST">										    	        
						<input type="submit" class="button my1" value="Авторизироваться"/>
					</form>
					<form action="../signup.php" method="POST">
						<input type="submit" class="button my2" value="Зарегистрироваться" />
					</form>   							    	       
					
				<?php endif;?>
				<?php 
					if($_SESSION['rate_error']==1){
						echo '<p>Ставка сделана.</p>' . 
							'Сумма ставки:'.($usrrate);
					}
					if($_SESSION['rate_error']==2){
						echo '<p style="color: red;">'.array_shift($errors).'</p><br>';
					}
				?>
			</div>
			<!-- КОНЕЦ ПЕРВЫЙ ЛЕВЫЙ БЛОК АВТОРИЗАЦИЯ И СТАВКИ -->
			
			<!-- ВТОРОЙ БЛОК САМ СТРИМ И ИНФОРМАЦИЯ -->
			<div class="my-flex-block2" >
						<h2 id="switch">Текущий матч:</h2>
						<h1><b id="rate_win">На победу: </b> <b id="rate_lose" style="margin-left: 15px;">На поражение: </b><b id="game_status" style="margin-left: 15px;"></b></h1>
					
					<div style="position: relative; padding-bottom: 56.25%;height: 0;">
						<iframe class="streamm" src="<?php echo $game->stream_url;?>" scrolling="no" style="position: absolute;top: 0;left: 0; width: 100%;height: 100%;" frameborder="0"></iframe>
					</div>
					<div class="text-flex-box">

											<h3>Дополнительная информация о стриме:</h3>
											<?php echo $game->about;?>
											

											<!-- <p id='json'>Дополнительной информации нет :)</p> -->
					</div>
					
			</div>
			<!--КОНЕЦ ВТОРОЙ БЛОК САМ СТРИМ И ИНФОРМАЦИЯ -->
			
			<!-- ТРЕТИЙ ПРАВЫЙ БЛОК ЧАТ -->
			<div class="my-flex-block3">
    			
        		<form id="loginform" align='middle' style="margin-left: 12%; margin-right: 12%;">
            	<input id="login" placeholder="Логин" class="input my1"><br>
            	<input id="password" placeholder="Пароль"  class="input my1"><br>
            	<button id="start" class="button my1">Войти</button>
        		</form>
        		<div id="mychatcontainer2">
        			<div id="chatContainer">
						<output  class="chat chat-55 rounded" id="messages"></output>
 					</div>
 				</div>
    			<div>
            	<input class="input my1" type='text' style="height: 45px;" id="chatinput" />
            </div>
        		<input type="submit" id='chatsubmit' class="button my1" style="height: 60px; " value="Отправить"/>
        		</div>
        	<!--КОНЕЦ ТРЕТИЙ ПРАВЫЙ БЛОК ЧАТ -->
			
			
			</div>
		 </div> 
					
				
								
		<!-- </div> -->



			<!-- Footer -->
				<?php require '../footer.php'?>

				


		<!-- Scripts -->
			<script src="../assets/js/jquery.min.js"></script>
			<script src="../assets/js/jquery.dropotron.min.js"></script>
			<script src="../assets/js/skel.min.js"></script>
			<script src="/assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script><?php echo 'var streamerlogin = "'.$streamer.'";'?></script>
			<script src="../assets/js/main.js"></script>			
			<script src="../ajaxclient.js"></script> 
			
			<!-- Chat scripts -->
			<script>
			$('#mychatcontainer2').height($(window).height()-200);
			</script>
          <script src="main.js" defer></script>
          

	</body>
</html>
