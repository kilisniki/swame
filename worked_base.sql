-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: worked_base
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `streamer` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stream_id` int(11) unsigned NOT NULL,
  `name` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `game` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'other',
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  `summ` int(10) unsigned DEFAULT '0',
  `date_begin` date DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `icon` varchar(140) COLLATE utf8mb4_unicode_ci DEFAULT 'shablon.png',
  `lose` int(11) unsigned DEFAULT '0',
  `win` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `streamer` (`streamer`),
  KEY `game` (`game`)
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES (22,'qzark11',11,'qzar','other',1,1,'2017-09-28','2017-09-29 00:00:00','shablon.png',1,1),(222,'qzark111',1111,'qzar','other',1,1,'2017-09-28','2017-09-29 00:00:00','shablon.png',1,1),(225,'qzark1',2,'qzark1(S)','overwatch',3,2,'2017-09-29','2018-09-12 19:05:12','shablon.png',0,0),(234,'streamer123',31,'streamer123','wot',16,0,'2018-09-13','2018-09-13 14:38:15','shablon.png',0,0);
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qzark1`
--

DROP TABLE IF EXISTS `qzark1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qzark1` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `summ` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qzark1`
--

LOCK TABLES `qzark1` WRITE;
/*!40000 ALTER TABLE `qzark1` DISABLE KEYS */;
INSERT INTO `qzark1` VALUES (1,0,'All rates',0,2,'2018-09-12 19:05:12',2),(2,0,'All rates',0,0,'2016-11-29 16:11:56',NULL),(3,0,'All rates',0,0,'2017-09-29 00:07:20',NULL),(4,0,'All rates',0,0,'2018-09-12 18:22:27',NULL),(5,30,'admin',0,2,'2018-09-12 18:18:06',NULL),(6,0,'All rates',0,0,'2018-09-12 19:03:23',NULL),(7,0,'All rates',0,0,'2018-09-12 19:05:49',NULL);
/*!40000 ALTER TABLE `qzark1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qzark11`
--

DROP TABLE IF EXISTS `qzark11`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qzark11` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `summ` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qzark11`
--

LOCK TABLES `qzark11` WRITE;
/*!40000 ALTER TABLE `qzark11` DISABLE KEYS */;
INSERT INTO `qzark11` VALUES (1,0,'All rates',3,6,'2016-11-19 18:50:07',9),(2,2,'qzark1',0,2,'2016-11-19 18:49:55',NULL),(3,2,'qzark1',0,4,'2016-11-19 18:50:02',NULL),(4,2,'qzark1',3,0,'2016-11-19 18:50:04',NULL);
/*!40000 ALTER TABLE `qzark11` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qzark12`
--

DROP TABLE IF EXISTS `qzark12`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qzark12` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qzark12`
--

LOCK TABLES `qzark12` WRITE;
/*!40000 ALTER TABLE `qzark12` DISABLE KEYS */;
INSERT INTO `qzark12` VALUES (1,0,'All rates',0,0,'2016-11-20 03:02:44');
/*!40000 ALTER TABLE `qzark12` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qzark13`
--

DROP TABLE IF EXISTS `qzark13`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qzark13` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qzark13`
--

LOCK TABLES `qzark13` WRITE;
/*!40000 ALTER TABLE `qzark13` DISABLE KEYS */;
INSERT INTO `qzark13` VALUES (1,0,'All rates',0,0,'2016-11-20 03:11:59');
/*!40000 ALTER TABLE `qzark13` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qzark14`
--

DROP TABLE IF EXISTS `qzark14`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qzark14` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qzark14`
--

LOCK TABLES `qzark14` WRITE;
/*!40000 ALTER TABLE `qzark14` DISABLE KEYS */;
INSERT INTO `qzark14` VALUES (1,0,'All rates',0,0,'2016-11-20 03:21:49');
/*!40000 ALTER TABLE `qzark14` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qzark15`
--

DROP TABLE IF EXISTS `qzark15`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qzark15` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qzark15`
--

LOCK TABLES `qzark15` WRITE;
/*!40000 ALTER TABLE `qzark15` DISABLE KEYS */;
INSERT INTO `qzark15` VALUES (1,0,'All rates',0,0,'2016-11-26 02:20:05');
/*!40000 ALTER TABLE `qzark15` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qzark16`
--

DROP TABLE IF EXISTS `qzark16`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qzark16` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qzark16`
--

LOCK TABLES `qzark16` WRITE;
/*!40000 ALTER TABLE `qzark16` DISABLE KEYS */;
INSERT INTO `qzark16` VALUES (1,0,'All rates',0,0,'2016-11-26 02:24:34');
/*!40000 ALTER TABLE `qzark16` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qzark17`
--

DROP TABLE IF EXISTS `qzark17`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qzark17` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qzark17`
--

LOCK TABLES `qzark17` WRITE;
/*!40000 ALTER TABLE `qzark17` DISABLE KEYS */;
INSERT INTO `qzark17` VALUES (1,0,'All rates',0,0,'2016-11-29 14:05:04');
/*!40000 ALTER TABLE `qzark17` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qzark18`
--

DROP TABLE IF EXISTS `qzark18`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qzark18` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qzark18`
--

LOCK TABLES `qzark18` WRITE;
/*!40000 ALTER TABLE `qzark18` DISABLE KEYS */;
INSERT INTO `qzark18` VALUES (1,0,'All rates',0,0,'2016-11-29 14:22:04');
/*!40000 ALTER TABLE `qzark18` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qzark19`
--

DROP TABLE IF EXISTS `qzark19`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qzark19` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qzark19`
--

LOCK TABLES `qzark19` WRITE;
/*!40000 ALTER TABLE `qzark19` DISABLE KEYS */;
INSERT INTO `qzark19` VALUES (1,0,'All rates',0,0,'2016-11-29 14:24:59');
/*!40000 ALTER TABLE `qzark19` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `streamer123`
--

DROP TABLE IF EXISTS `streamer123`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `streamer123` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `summ` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `streamer123`
--

LOCK TABLES `streamer123` WRITE;
/*!40000 ALTER TABLE `streamer123` DISABLE KEYS */;
INSERT INTO `streamer123` VALUES (1,0,'All rates',0,5,'2018-09-13 14:37:39',5),(2,0,'All rates',0,0,'2018-09-13 14:37:50',NULL),(3,30,'admin',0,5,'2018-09-13 14:33:00',NULL),(4,0,'All rates',0,0,'2018-09-13 14:38:15',NULL);
/*!40000 ALTER TABLE `streamer123` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `streamer12310`
--

DROP TABLE IF EXISTS `streamer12310`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `streamer12310` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `streamer12310`
--

LOCK TABLES `streamer12310` WRITE;
/*!40000 ALTER TABLE `streamer12310` DISABLE KEYS */;
INSERT INTO `streamer12310` VALUES (1,0,'All rates',0,0,'2018-09-12 19:17:05');
/*!40000 ALTER TABLE `streamer12310` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `streamer12311`
--

DROP TABLE IF EXISTS `streamer12311`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `streamer12311` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `summ` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `streamer12311`
--

LOCK TABLES `streamer12311` WRITE;
/*!40000 ALTER TABLE `streamer12311` DISABLE KEYS */;
INSERT INTO `streamer12311` VALUES (1,0,'All rates',0,4,'2018-09-12 20:16:35',4),(2,30,'admin',0,4,'2018-09-12 20:12:54',NULL);
/*!40000 ALTER TABLE `streamer12311` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `streamer12313`
--

DROP TABLE IF EXISTS `streamer12313`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `streamer12313` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `summ` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `streamer12313`
--

LOCK TABLES `streamer12313` WRITE;
/*!40000 ALTER TABLE `streamer12313` DISABLE KEYS */;
INSERT INTO `streamer12313` VALUES (1,0,'All rates',5,1,'2018-09-12 20:54:56',6),(2,30,'admin',0,1,'2018-09-12 20:54:02',NULL),(3,30,'admin',5,0,'2018-09-12 20:54:06',NULL);
/*!40000 ALTER TABLE `streamer12313` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `streamer12314`
--

DROP TABLE IF EXISTS `streamer12314`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `streamer12314` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `summ` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `streamer12314`
--

LOCK TABLES `streamer12314` WRITE;
/*!40000 ALTER TABLE `streamer12314` DISABLE KEYS */;
INSERT INTO `streamer12314` VALUES (1,0,'All rates',6,5,'2018-09-13 14:31:28',11),(2,30,'admin',0,5,'2018-09-12 21:24:43',NULL),(3,30,'admin',6,0,'2018-09-12 21:24:52',NULL);
/*!40000 ALTER TABLE `streamer12314` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `streamer12315`
--

DROP TABLE IF EXISTS `streamer12315`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `streamer12315` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `streamer12315`
--

LOCK TABLES `streamer12315` WRITE;
/*!40000 ALTER TABLE `streamer12315` DISABLE KEYS */;
INSERT INTO `streamer12315` VALUES (1,0,'All rates',0,0,'2018-09-13 14:36:34');
/*!40000 ALTER TABLE `streamer12315` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `streamers`
--

DROP TABLE IF EXISTS `streamers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `streamers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stream_id` int(11) NOT NULL,
  `login` varchar(70) NOT NULL,
  `name` varchar(70) NOT NULL,
  `stream_url` varchar(150) DEFAULT NULL,
  `up_cache` int(10) unsigned NOT NULL DEFAULT '0',
  `contacts` varchar(150) DEFAULT NULL,
  `confidence` int(2) unsigned NOT NULL DEFAULT '1',
  `about` text,
  `games_played` int(10) unsigned NOT NULL DEFAULT '0',
  `games_win` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` date DEFAULT NULL,
  `online` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `streamers`
--

LOCK TABLES `streamers` WRITE;
/*!40000 ALTER TABLE `streamers` DISABLE KEYS */;
INSERT INTO `streamers` VALUES (1,27,'qzark1(S)','qzark1(S)','Ссылка на стрим',0,'Контакты',1,'О стриме',0,0,'2016-10-30',0),(2,1,'ourcash','ourcash',NULL,0,NULL,1,NULL,0,0,NULL,0),(3,2,'qzark1','qzark1(S)','',0,'Контакты',1,'<h3>Добро пожаловать, гайсы.</h3>\r\n<p>Стримы ежедневно с 15-00 до 17-00. <b>Смотреть обязательно.</b></p> Почему не переводится текст на следующую строку я не знаю. Честно.<p><i>А нет, все нормально.</i></p>\r\n\r\n<p>Посмотрим как это все выглядит.</p>',0,0,'2016-10-30',0),(4,31,'streamer123','streamer123','https://player.twitch.tv/?channel=gamelifeow',0,NULL,10,NULL,0,0,'2018-09-12',0);
/*!40000 ALTER TABLE `streamers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `swamestream`
--

DROP TABLE IF EXISTS `swamestream`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `swamestream` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) unsigned DEFAULT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lose` int(11) unsigned DEFAULT NULL,
  `win` int(11) unsigned DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `swamestream`
--

LOCK TABLES `swamestream` WRITE;
/*!40000 ALTER TABLE `swamestream` DISABLE KEYS */;
INSERT INTO `swamestream` VALUES (1,2,'qzark1',0,0,'2016-11-19 01:04:46');
/*!40000 ALTER TABLE `swamestream` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_registration` datetime DEFAULT NULL,
  `stars` double unsigned DEFAULT '0',
  `access` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `steam64` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `code` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'ourcash','ourcash@swame.ru','123',NULL,7.6,'streamer','0',NULL),(2,'qzark1','','$2y$10$pwxuAXBHvDS5AjCmqq92vuCmi3eVxXQcS.Y1KVxf/gn.oE21GS6Q6','2016-10-24 17:43:44',95.4,'admin','0',4840653),(3,'Beyronk','','$2y$10$pwxuAXBHvDS5AjCmqq92vuCmi3eVxXQcS.Y1KVxf/gn.oE21GS6Q6','2016-10-24 22:05:21',0,'usertostrea','0',1068608),(4,'String<script>alert(\'жопа\')</script>','loscript@ya.com','$2y$10$mlWqsCaD.r4p2U2PDwt48.zuu2xMvoLCi1L.7WBzXW5gXap9EwXHG','2016-10-28 19:48:17',0,'user','0',NULL),(9,'test1','test1@swame.ru','$2y$10$tmmUt4ke7NnQtEcIOf2hjeE0NX1kSj375zQ3j5UGQ0425azdxyfi.','2016-10-29 13:20:55',0,'user','0',1768782),(27,'qzark1(S)','0','$2y$10$pwxuAXBHvDS5AjCmqq92vuCmi3eVxXQcS.Y1KVxf/gn.oE21GS6Q6','2016-10-30 16:10:52',0,'streamer','76561198110793845',NULL),(28,'streamer','streamer@swame.ru','$2y$10$PSZBE5rCnclde0KCwM6y...1sgrZSzHVyob09T2jmLsW6/zdNf1Qy','2016-11-05 16:14:51',0,'usertostrea','0',3506733),(29,'swamestreamer','swame@streamer.ruu','$2y$10$DW0SxUuF/FBOyGY5D/TvsuUp7shQ.x.3MmCsqqC7./aYAyquC68dq','2016-11-23 01:36:22',0,'user','0',6493740),(30,'admin','admin@admin.ru','$2y$10$pwxuAXBHvDS5AjCmqq92vuCmi3eVxXQcS.Y1KVxf/gn.oE21GS6Q6','2018-09-12 17:42:30',990,'admin','0',675963),(31,'streamer123','streamer123@stream.ru','$2y$10$rtjJss50vajIeDYRZGIAveZGMnSv1pAm/.upmJsZdZAtUTeadAa9i','2018-09-12 19:04:57',100,'streamer','0',7025483);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webchat_lines`
--

DROP TABLE IF EXISTS `webchat_lines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webchat_lines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author` varchar(16) NOT NULL,
  `gravatar` varchar(32) NOT NULL,
  `text` varchar(255) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `ts` (`ts`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webchat_lines`
--

LOCK TABLES `webchat_lines` WRITE;
/*!40000 ALTER TABLE `webchat_lines` DISABLE KEYS */;
/*!40000 ALTER TABLE `webchat_lines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webchat_users`
--

DROP TABLE IF EXISTS `webchat_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webchat_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `gravatar` varchar(32) NOT NULL,
  `last_activity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `last_activity` (`last_activity`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webchat_users`
--

LOCK TABLES `webchat_users` WRITE;
/*!40000 ALTER TABLE `webchat_users` DISABLE KEYS */;
INSERT INTO `webchat_users` VALUES (23,'qzark1','','2016-11-15 00:38:54');
/*!40000 ALTER TABLE `webchat_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-13 15:13:29
